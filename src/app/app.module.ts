import { HttpClient, HttpClientModule } from '@angular/common/http';

import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';

import { BrowserModule } from '@angular/platform-browser';



import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { AppService } from './app.service';



@NgModule({

  declarations: [

    AppComponent

  ],

  imports: [

    FormsModule,

    HttpClientModule,

    FormsModule,

    BrowserModule,

    AppRoutingModule

  ],

  providers: [AppService, HttpClient],

  bootstrap: [AppComponent]

})

export class AppModule { }