import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import io from 'socket.io-client';
import { Observable,  } from 'rxjs';

 



@Injectable({

    providedIn: 'root'

})

export class AppService {
  socket:any;
  readonly url:string="ws://localhost:4000/mytest" 
    constructor(private http: HttpClient){
      this.socket= io(this.url)
    }
    
     
    listen(eventName:string){
     return  new Observable((Subscriber)=>{
        this.socket.on(eventName, (data:any)=>{
          Subscriber.next(data);
    
      })
    });
  }
  emit(eventName:string,data:any){
    this.socket.emit(eventName,data)
  }

    test() {//useless prolly

        return this.http.get<any>("http://localhost:4000/mytest");

    }

    test2() {//useless prolly

        return this.http.get<any>("http://localhost:4000");

    }

}